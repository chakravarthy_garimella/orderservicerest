# My Cart

## Requirement
### We have an Order object
#### Order
* order Id - String
* List<Items> - List
* Shipping Address - String
* Person - Object
#### Item
* item id
* quantity
#### Person
* name
* phone no
#### The candidate needs to write code to perform CRUD operations on the order objects with the below expectations:
* Expose a REST endpoint to perform GET/POST/DELETE/PUT operations on the order
* For REPLACE opeations, provide facitiy to update address and item quantity.
* The operations shoud work for both relational and non-relational (document db preferrably) databases based on a configuration value change
* Write junits to test your code
#### Other Details
* Use any framework of choice
* Use any data bases of choice ( relational / non-relational )

## Maven Command
```
mvn archetype:generate -DgroupId=com.my.cart -DartifactId=my-cart -Dversion=1.0 -DarchetypeArtifactId=maven-archetype-quickstart -DinteractiveMode=false
```

## Steps
* Dependencies - Refer [pom.xml](pom.xml)

## API
* Postman collection - [files/my-cart.postman_collection.json](files/my-cart.postman_collection.json)
* Postman collection environment - [files/my-cart.postman_environment.json](files/my-cart.postman_environment.json)
* Swagger - http://localhost:9000/swagger-ui.html

## Actuator
* Any of the following approaches can be used to access actuator end points
    * Refer `actuator` folder in postman collection
    * Open URL - http://localhost:9000/browser/index.html
        * Enter `/actuator` in `Explorer` text box

## MySQL Schema
* Create DB - [mysql-database/database.sql](database.sql)
* Create tables - [mysql-database/schema/schema-1.sql](mysql-database/schema/schema-1.sql)
* Selecte tables - [mysql-database/select.sql](mysql-database/select.sql)
* Selecte tables - [mysql-database/drop.sql](mysql-database/drop.sql)

## Run with MySQL DB
* Run below command from repo directory. Runs on port `9000`
```
mvn spring-boot:run -Dspring-boot.run.arguments=--spring.profiles.active=mysql
```

## Run with MongoDB
* Run below command from repo directory. Runs on port `9001`
```
mvn spring-boot:run -Dspring-boot.run.arguments=--spring.profiles.active=mongodb
```