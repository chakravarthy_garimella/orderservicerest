-- create db and user
create database if not exists mycart;
create user 'mycart_user1'@'localhost' identified by 'mycart_password1';
grant all on mycart.* to 'mycart_user1'@'localhost';

-- drop user and database
drop user 'mycart_user1'@'localhost';
drop database mycart;