use mycart;

-- t_person
select * from t_person;
delete from t_person;
commit;

-- t_order
select * from t_order;
delete from t_order;
commit;

-- t_item
select * from t_item;
delete from t_item;
commit;