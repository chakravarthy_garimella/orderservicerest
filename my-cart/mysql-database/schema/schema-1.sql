use mycart;

create table t_person
(
	id varchar(50),
    name varchar(50),
    phone_number bigint,
    primary key(id)
);

create table t_order
(
	id varchar(50),
    shipping_address varchar(500),
    person_id varchar(50),
    primary key(id),
    constraint order_person_id_fk foreign key(person_id) references t_person(id) on delete set null
);

create table t_item
(
	id varchar(50),
    quantity bigint,
    order_id varchar(50),
    primary key(id),
    constraint item_order_id_fk foreign key(order_id) references t_order(id) on delete set null
);