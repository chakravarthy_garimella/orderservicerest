package com.my.cart.service;

import static com.my.cart.util.ErrorsEnum.NULL_PROPERTY;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.my.cart.entity.mongo.ItemMongoEntity;
import com.my.cart.entity.mongo.OrderMongoEntity;
import com.my.cart.entity.mongo.PersonMongoEntity;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.entity.PersonEntity;
import com.my.cart.model.ItemModel;
import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.repository.OrderMongoRepository;
import com.my.cart.repository.OrderRepository;
import com.my.cart.repository.PersonMongoRepository;
import com.my.cart.repository.PersonRepository;
import com.my.cart.util.Constants;
import com.my.cart.util.Utils;

import lombok.SneakyThrows;

@Service
public class OrderService {

	@Autowired
	private Utils utils;

	@Autowired
	private SearchService searchService;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private PersonMongoRepository personMongoRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderMongoRepository orderMongoRepository;

	@Value("${app.use.mongo}")
	private Boolean useMongo;

	public PersonEntity savePersonEntity(PersonEntity personEntity) {
		if (useMongo) {
			PersonMongoEntity personMongoEntity = personEntity.buildMongoEntity();
			personMongoEntity = personMongoRepository.save(personMongoEntity);
			return personMongoEntity.buildEntity();
		} else {
			 return personRepository.save(personEntity);
		}
	}

	public void deletePersonEntity(PersonEntity personEntity) {
		if (useMongo) {
			personMongoRepository.delete(personEntity.buildMongoEntity());
		} else {
			personRepository.delete(personEntity);
		}
	}

	public PersonModel savePersonModel(PersonModel personModel) {
		PersonEntity personEntity = personModel.buildEntity();
		PersonEntity persistedPersonEntity = savePersonEntity(personEntity);
		return persistedPersonEntity.buildModel();
	}

	@SneakyThrows
	public PersonModel updatePersonModel(PersonModel personModel) {
		String personId = Optional.ofNullable(personModel.getId())
				.orElseThrow(utils.buildAppException(NULL_PROPERTY.getCode(), NULL_PROPERTY.getMessage(), null,
						HttpStatus.BAD_REQUEST, "personModel", Constants.ID));
		PersonEntity personEntity = searchService.findPersonEntityById(personId);
		personModel.updateEntity(personEntity);
		PersonEntity persistedPersonEntity = savePersonEntity(personEntity);
		return persistedPersonEntity.buildModel();
	}

	@SneakyThrows
	public PersonModel deletePersonModel(String personId) {
		personId = Optional.ofNullable(personId).orElseThrow(utils.buildAppException(NULL_PROPERTY.getCode(),
				NULL_PROPERTY.getMessage(), null, HttpStatus.BAD_REQUEST, "personModel", Constants.ID));
		PersonEntity personEntity = searchService.findPersonEntityById(personId);
		deletePersonEntity(personEntity);
		return personEntity.buildModel();
	}

	public OrderEntity saveOrderEntity(OrderEntity orderEntity) {
		if (useMongo) {
			OrderMongoEntity orderMongoEntity = orderEntity.buildMongoEntity();
			return orderMongoRepository.save(orderMongoEntity).buildEntity();
		} else {
			 return orderRepository.save(orderEntity);
		}
	}

	public OrderModel saveOrderModel(OrderModel orderModel) {
		OrderEntity orderEntity = orderModel.buildEntity();

		PersonModel personModel = orderModel.getPersonModel();
		if (null != personModel && StringUtils.isNotBlank(personModel.getId())) {
			orderEntity.setPersonEntity(searchService.findPersonEntityById(personModel.getId()));
		}

		List<ItemModel> itemModelList = orderModel.getItemModelList();
		if (CollectionUtils.isNotEmpty(itemModelList)) {
			List<ItemEntity> itemEntityList = itemModelList.stream().map(itemModel -> {
				ItemEntity itemEntity = itemModel.buildEntity();
				itemEntity.setOrderEntity(orderEntity);
				return itemEntity;
			}).collect(Collectors.toList());
			orderEntity.setItemEntityList(itemEntityList);
		}

		OrderEntity persistedOrderEntity = saveOrderEntity(orderEntity);

		return persistedOrderEntity.buildFullModel();
	}

	@SneakyThrows
	public OrderModel updateOrderModel(OrderModel orderModel) {
		String orderModelId = Optional.ofNullable(orderModel.getId())
				.orElseThrow(utils.buildAppException(NULL_PROPERTY.getCode(), NULL_PROPERTY.getMessage(), null,
						HttpStatus.BAD_REQUEST, OrderModel.class.getSimpleName(), Constants.ID));
		OrderEntity orderEntity = searchService.findOrderEntityById(orderModelId);
		orderModel.updateEntity(orderEntity);

		// update PersonEntity
		PersonModel personModel = orderModel.getPersonModel();
		if (null != personModel && StringUtils.isNotBlank(personModel.getId())) {
			PersonEntity personEntity = searchService.findPersonEntityById(personModel.getId());
			orderEntity.setPersonEntity(personEntity);
		}

		// update ItemEntity list
		List<ItemModel> itemModelList = orderModel.getItemModelList();
		if (CollectionUtils.isNotEmpty(itemModelList)) {
			List<ItemEntity> itemEntityList = itemModelList.stream().map(itemModel -> {
				// TODO validation.. can do single select rather than calling find in loop
				ItemEntity itemEntity = useMongo ? itemModel.buildEntity() : searchService.findItemEntityById(itemModel.getId());
				itemEntity.setQuantity(itemModel.getQuantity());
				return itemEntity;
			}).collect(Collectors.toList());
			orderEntity.setItemEntityList(itemEntityList);
		}

		OrderEntity updatedOrderEntity = saveOrderEntity(orderEntity);

		return updatedOrderEntity.buildFullModel();
	}

	public void deleteOrderEntity(OrderEntity orderEntity) {
		if (useMongo) {
			OrderMongoEntity orderMongoEntity = orderEntity.buildMongoEntity();
			orderMongoRepository.delete(orderMongoEntity);
		} else {
			orderRepository.delete(orderEntity);
		}
	}

	public OrderModel deleteOrderModel(String orderId) {
		OrderEntity orderEntity = searchService.findOrderEntityById(orderId);
		orderEntity.setPersonEntity(null);
		deleteOrderEntity(orderEntity);
		return orderEntity.buildFullModel();
	}

}