package com.my.cart.service;

import static com.my.cart.util.ErrorsEnum.OBJECT_NOT_FOUND;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.my.cart.entity.mongo.ItemMongoEntity;
import com.my.cart.entity.mongo.OrderMongoEntity;
import com.my.cart.entity.mongo.PersonMongoEntity;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.ListUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.entity.PersonEntity;
import com.my.cart.model.ItemModel;
import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.model.ResponseModel;
import com.my.cart.repository.ItemMongoRepository;
import com.my.cart.repository.ItemRepository;
import com.my.cart.repository.OrderMongoRepository;
import com.my.cart.repository.OrderRepository;
import com.my.cart.repository.PersonMongoRepository;
import com.my.cart.repository.PersonRepository;
import com.my.cart.util.Constants;
import com.my.cart.util.Utils;

import lombok.SneakyThrows;

@Service
public class SearchService {

	@Autowired
	private Utils utils;

	@Autowired
	private PersonRepository personRepository;

	@Autowired
	private PersonMongoRepository personMongoRepository;

	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private OrderMongoRepository orderMongoRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Autowired
	private ItemMongoRepository itemMongoRepository;

	@Value("${app.use.mongo}")
	private Boolean useMongo;

	@SneakyThrows
	public PersonEntity findPersonEntityById(String personId) {
		if (useMongo) {
			PersonMongoEntity personMongoEntity = personMongoRepository.findById(personId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							personId, HttpStatus.BAD_REQUEST, PersonModel.class.getSimpleName(), Constants.ID));
			return personMongoEntity.buildEntity();
		} else {
			return personRepository.findById(personId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							personId, HttpStatus.BAD_REQUEST, PersonModel.class.getSimpleName(), Constants.ID));
		}
	}

	public PersonModel findPersonModelById(String personId) {
		return findPersonEntityById(personId).buildModel();
	}

	public ResponseModel findPersonModelList() {
		if (useMongo) {
			List<PersonMongoEntity> personMongoEntityList = personMongoRepository.findAll();
			List<PersonEntity> personEntityList = personMongoEntityList.stream().map(PersonMongoEntity::buildEntity).collect(Collectors.toList());
			List<PersonModel> personModelList = CollectionUtils.isEmpty(personEntityList) ? new ArrayList<>()
					: personEntityList.stream().map(PersonEntity::buildModel).collect(Collectors.toList());
			return utils.buildResponseModel(personModelList, personEntityList.size(), personModelList.size());
		} else {
			List<PersonEntity> personEntityList = personRepository.findAll();
			List<PersonModel> personModelList = CollectionUtils.isEmpty(personEntityList) ? new ArrayList<>()
					: personEntityList.stream().map(PersonEntity::buildModel).collect(Collectors.toList());
			return utils.buildResponseModel(personModelList, personEntityList.size(), personModelList.size());
		}
	}

	public List<OrderEntity> findAllOrderEntityList() {
		if (useMongo) {
			 List<OrderMongoEntity> orderMongoEntityList = ListUtils.emptyIfNull(orderMongoRepository.findAll());
			 return orderMongoEntityList.stream().map(OrderMongoEntity::buildEntity).collect(Collectors.toList());
		} else {
			 return ListUtils.emptyIfNull(orderRepository.findAll());
		}
	}

	public ResponseModel findAllOrderModelList() {
		List<OrderEntity> orderEntityList = findAllOrderEntityList();
		List<OrderModel> orderModelList = orderEntityList.stream().map(OrderEntity::buildFullModel)
				.collect(Collectors.toList());
		return utils.buildResponseModel(orderModelList, orderModelList.size(), orderModelList.size());
	}

	@SneakyThrows
	public OrderEntity findOrderEntityById(String orderId) {
		if (useMongo) {
			OrderMongoEntity orderMongoEntity = orderMongoRepository.findById(orderId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							orderId, HttpStatus.BAD_REQUEST, OrderModel.class.getSimpleName(), Constants.ID));
			return orderMongoEntity.buildEntity();
		} else {
			return orderRepository.findById(orderId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							orderId, HttpStatus.BAD_REQUEST, OrderModel.class.getSimpleName(), Constants.ID));
		}
	}

	@SneakyThrows
	public ItemEntity findItemEntityById(String itemId) {
		if (useMongo) {
			ItemMongoEntity itemMongoEntity = itemMongoRepository.findById(itemId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							itemId, HttpStatus.BAD_REQUEST, ItemModel.class.getSimpleName(), Constants.ID));
			return itemMongoEntity.buildEntity();
		} else {
			return itemRepository.findById(itemId)
					.orElseThrow(utils.buildAppException(OBJECT_NOT_FOUND.getCode(), OBJECT_NOT_FOUND.getMessage(),
							itemId, HttpStatus.BAD_REQUEST, ItemModel.class.getSimpleName(), Constants.ID));
		}

	}

	public OrderModel findOrderModelById(String orderId) {
		OrderEntity orderEntity = findOrderEntityById(orderId);
		return orderEntity.buildFullModel();
	}
}