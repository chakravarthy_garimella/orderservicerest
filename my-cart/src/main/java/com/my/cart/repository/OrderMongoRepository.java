package com.my.cart.repository;

import com.my.cart.entity.mongo.OrderMongoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.my.cart.entity.OrderEntity;

//@Repository
public interface OrderMongoRepository extends MongoRepository<OrderMongoEntity, String> {

}