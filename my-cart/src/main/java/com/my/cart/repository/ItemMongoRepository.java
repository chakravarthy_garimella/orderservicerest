package com.my.cart.repository;

import com.my.cart.entity.mongo.ItemMongoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.my.cart.entity.ItemEntity;

@Repository
public interface ItemMongoRepository extends MongoRepository<ItemMongoEntity, String> {

}