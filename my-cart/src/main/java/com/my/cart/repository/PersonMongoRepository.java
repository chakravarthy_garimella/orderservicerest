package com.my.cart.repository;

import com.my.cart.entity.mongo.PersonMongoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.my.cart.entity.PersonEntity;

@Repository
public interface PersonMongoRepository extends MongoRepository<PersonMongoEntity, String> {

}