package com.my.cart.model;

import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.my.cart.entity.ItemEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemModel {

	private String id;
	private Long quantity;
	private OrderModel orderModel;

	@SneakyThrows
	public ItemEntity buildEntity() {
		ItemEntity itemEntity = new ItemEntity();
		BeanUtils.copyProperties(itemEntity, this);
		return itemEntity;
	}

	public void updateEntity(ItemEntity itemEntity) {
		itemEntity.setQuantity(quantity);
	}

}