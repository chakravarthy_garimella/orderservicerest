package com.my.cart.model.exception;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class AppException extends RuntimeException {

	private static final long serialVersionUID = 4182294436606075029L;
	private Object errors;

	@Override
	public String getMessage() {
		return errors.toString();
	}
}