package com.my.cart.model;

import org.apache.commons.beanutils.BeanUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.my.cart.entity.PersonEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonModel {

	private String id;
	private String name;
	private Long phoneNumber;

	@SneakyThrows
	public PersonEntity buildEntity() {
		PersonEntity personEntity = new PersonEntity();
		BeanUtils.copyProperties(personEntity, this);
		return personEntity;
	}

	public void updateEntity(PersonEntity personEntity) {
		personEntity.setName(name);
		personEntity.setPhoneNumber(phoneNumber);
	}

}