package com.my.cart.model;

import java.util.List;

import org.apache.commons.beanutils.BeanUtils;

import com.my.cart.entity.OrderEntity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = "id")
public class OrderModel {

	private String id;
	private String shippingAddress;
	private List<ItemModel> itemModelList;
	private PersonModel personModel;

	@SneakyThrows
	public OrderEntity buildEntity() {
		OrderEntity orderEntity = new OrderEntity();
		BeanUtils.copyProperties(orderEntity, this);
		return orderEntity;
	}

	public void updateEntity(OrderEntity orderEntity) {
		orderEntity.setShippingAddress(shippingAddress);
	}

}