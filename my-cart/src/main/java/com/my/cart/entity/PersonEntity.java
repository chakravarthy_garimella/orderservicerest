package com.my.cart.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.my.cart.entity.mongo.PersonMongoEntity;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.annotations.GenericGenerator;

import com.my.cart.model.PersonModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = "id")
@Entity
@Table(name = "t_person")
//@Document(collection = "person")
//@Document("person")
public class PersonEntity {

	@Id
	@GenericGenerator(name = "custom-primary-key-generator", strategy = "com.my.cart.util.PrimaryKeyGenerator")
	@GeneratedValue(generator = "custom-primary-key-generator")
	@Column(name = "id")
	private String id;

	@Column(name = "name")
	private String name;

	@Column(name = "phone_number")
	private Long phoneNumber;

	@SneakyThrows
	public PersonModel buildModel() {
		PersonModel personModel = new PersonModel();
		BeanUtils.copyProperties(personModel, this);
		return personModel;
	}

	@SneakyThrows
	public PersonMongoEntity buildMongoEntity(){
		PersonMongoEntity personMongoEntity = new PersonMongoEntity();
		personMongoEntity.setId(id);
		personMongoEntity.setName(name);
		personMongoEntity.setPhoneNumber(phoneNumber);
		return personMongoEntity;
	}

}
