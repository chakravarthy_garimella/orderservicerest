package com.my.cart.entity.mongo;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.model.ItemModel;
import lombok.*;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "item")
public class ItemMongoEntity {

	@Id
	private String id;
	private Long quantity;
	private OrderEntity orderEntity;

	@SneakyThrows
	public ItemEntity buildEntity(){
		ItemEntity itemEntity = new ItemEntity();
		itemEntity.setId(id);
		itemEntity.setQuantity(quantity);

		return itemEntity;
	}

}
