package com.my.cart.entity;

import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.my.cart.entity.mongo.ItemMongoEntity;
import com.my.cart.entity.mongo.OrderMongoEntity;
import com.my.cart.entity.mongo.PersonMongoEntity;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.GenericGenerator;

import com.my.cart.model.OrderModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;
import org.springframework.context.annotation.Bean;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = "id")
@Entity
@Table(name = "t_order")
public class OrderEntity {

	@Id
	@GenericGenerator(name = "custom-primary-key-generator", strategy = "com.my.cart.util.PrimaryKeyGenerator")
	@GeneratedValue(generator = "custom-primary-key-generator")
	@Column(name = "id")
	private String id;

	@Column(name = "shipping_address")
	private String shippingAddress;

	@OneToMany(mappedBy = "orderEntity", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<ItemEntity> itemEntityList;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "person_id", referencedColumnName = "id")
	private PersonEntity personEntity;

	@SneakyThrows
	public OrderModel buildModel() {
		OrderModel orderModel = new OrderModel();
		BeanUtils.copyProperties(orderModel, this);
		return orderModel;
	}

	public OrderModel buildFullModel() {
		OrderModel orderModel = buildModel();

		if (CollectionUtils.isNotEmpty(itemEntityList)) {
			orderModel
					.setItemModelList(itemEntityList.stream().map(ItemEntity::buildModel).collect(Collectors.toList()));
		}

		if (null != personEntity) {
			orderModel.setPersonModel(personEntity.buildModel());
		}

		return orderModel;
	}

	@SneakyThrows
	public OrderMongoEntity buildMongoEntity(){
		OrderMongoEntity orderMongoEntity = new OrderMongoEntity();
		orderMongoEntity.setId(id);
		orderMongoEntity.setShippingAddress(shippingAddress);

		if(null != personEntity) {
			PersonMongoEntity personMongoEntity = personEntity.buildMongoEntity();
			orderMongoEntity.setPersonEntity(personMongoEntity);
		}

		if(CollectionUtils.isNotEmpty(itemEntityList)){
			List<ItemMongoEntity> itemMongoEntityList = itemEntityList.stream().map(ItemEntity::buildMongoEntity).collect(Collectors.toList());
			orderMongoEntity.setItemEntityList(itemMongoEntityList);
		}

		return orderMongoEntity;
	}

}
