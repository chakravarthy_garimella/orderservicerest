package com.my.cart.entity.mongo;

import com.my.cart.entity.PersonEntity;
import com.my.cart.model.PersonModel;
import lombok.*;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "person", value = "person")
public class PersonMongoEntity {

	@Id
	private String id;
	private String name;
	private Long phoneNumber;

	@SneakyThrows
	public PersonEntity buildEntity(){
		PersonEntity personEntity = new PersonEntity();
		personEntity.setId(id);
		personEntity.setName(name);
		personEntity.setPhoneNumber(phoneNumber);
		return  personEntity;
	}
}
