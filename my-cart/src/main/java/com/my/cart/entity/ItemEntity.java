package com.my.cart.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.my.cart.entity.mongo.ItemMongoEntity;
import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.annotations.GenericGenerator;

import com.my.cart.model.ItemModel;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import lombok.ToString;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(of = "id")
@Entity
@Table(name = "t_item")
public class ItemEntity {

	@Id
	@GenericGenerator(name = "custom-primary-key-generator", strategy = "com.my.cart.util.PrimaryKeyGenerator")
	@GeneratedValue(generator = "custom-primary-key-generator")
	@Column(name = "id")
	private String id;

	@Column(name = "quantity")
	private Long quantity;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "order_id", referencedColumnName = "id")
	private OrderEntity orderEntity;

	@SneakyThrows
	public ItemModel buildModel() {
		ItemModel itemModel = new ItemModel();
		BeanUtils.copyProperties(itemModel, this);
		return itemModel;
	}

	public ItemModel buildFullModel() {
		ItemModel itemModel = buildModel();

		if (null != orderEntity) {
			itemModel.setOrderModel(orderEntity.buildModel());
		}
		return itemModel;
	}

	@SneakyThrows
	public ItemMongoEntity buildMongoEntity(){
		ItemMongoEntity itemMongoEntity = new ItemMongoEntity();
		itemMongoEntity.setId(id);
		itemMongoEntity.setQuantity(quantity);

		return itemMongoEntity;
	}

}
