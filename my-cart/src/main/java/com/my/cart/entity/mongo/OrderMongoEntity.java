package com.my.cart.entity.mongo;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.model.OrderModel;
import lombok.*;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "order", value = "order")
public class OrderMongoEntity {

	@Id
	private String id;
	private String shippingAddress;
	private List<ItemMongoEntity> itemEntityList;
	private PersonMongoEntity personEntity;

	@SneakyThrows
	public OrderEntity buildEntity(){
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setId(id);
		orderEntity.setShippingAddress(shippingAddress);

		if(null != personEntity){
			orderEntity.setPersonEntity(personEntity.buildEntity());
		}

		if(CollectionUtils.isNotEmpty(itemEntityList)){
			orderEntity.setItemEntityList(itemEntityList.stream().map(ItemMongoEntity::buildEntity).collect(Collectors.toList()));
		}

		return orderEntity;
	}

}
