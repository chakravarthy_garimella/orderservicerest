package com.my.cart.aspect;

import java.sql.Timestamp;
import java.time.LocalDateTime;

import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.ThreadContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.my.cart.model.exception.AppException;
import com.my.cart.model.exception.ErrorModel;
import com.my.cart.model.exception.ExceptionModel;
import com.my.cart.util.Constants;
import com.my.cart.util.ErrorsEnum;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private HttpServletResponse httpServletResponse;

	@ExceptionHandler(value = { AppException.class })
	protected ResponseEntity<Object> handleException(AppException runtimeException, WebRequest webRequest) {
		ExceptionModel exceptionModel = null;
		try {
			log.error(ErrorsEnum.BAD_REQUEST_EXCEPTION.getMessage(), runtimeException);
			exceptionModel = ExceptionModel.builder().errors(runtimeException.getErrors()).build();
			httpServletResponse.setHeader(Constants.CORRELATION_ID, ThreadContext.get(Constants.CORRELATION_ID));
		} catch (Exception e) {
			log.error(ErrorsEnum.ERROR_WHILE_PARSING_ERROR_MESSAGE.getMessage(), e);
			ErrorModel errorModel = ErrorModel.builder().code(ErrorsEnum.ERROR_WHILE_PARSING_ERROR_MESSAGE.getCode())
					.message(ErrorsEnum.ERROR_WHILE_PARSING_ERROR_MESSAGE.getMessage()).build();
			exceptionModel = ExceptionModel.builder().errors(errorModel).build();
		}

		return handleExceptionInternal(runtimeException, exceptionModel, new HttpHeaders(), HttpStatus.BAD_REQUEST,
				webRequest);
	}

	/**
	 * Global exception other than above 3 exceptions
	 * @param
	 *
	 * @return
	 */
	@ExceptionHandler(value = { Exception.class })
	public ResponseEntity<Object> otherExceptions(Exception exception, WebRequest webRequest) {
		log.error(ErrorsEnum.BAD_REQUEST_EXCEPTION.getMessage(), exception);
		ExceptionModel exceptionModel = ExceptionModel.builder()
				.errors(exception.getClass().getName() + " : " + exception.getMessage()).build();
		httpServletResponse.setHeader(Constants.CORRELATION_ID, ThreadContext.get(Constants.CORRELATION_ID));
		return handleExceptionInternal(exception, exceptionModel, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR,
				webRequest);
	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		httpServletResponse.setHeader(Constants.CORRELATION_ID, ThreadContext.get(Constants.CORRELATION_ID));
		ErrorModel errorModel = ErrorModel.builder().value(ex.getBindingResult())
				.timestamp(Timestamp.valueOf(LocalDateTime.now()).getTime()).status(HttpStatus.BAD_REQUEST.value())
				.build();
		httpServletResponse.setHeader(Constants.CORRELATION_ID, ThreadContext.get(Constants.CORRELATION_ID));
		return new ResponseEntity<>(errorModel, HttpStatus.BAD_REQUEST);
	}

}