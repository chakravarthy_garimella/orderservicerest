package com.my.cart.util;

import java.sql.Timestamp;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;
import java.util.function.Supplier;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.my.cart.model.ResponseModel;
import com.my.cart.model.exception.AppException;
import com.my.cart.model.exception.ErrorModel;

@SuppressWarnings("all")
@Component
public class Utils {

	@Autowired
	private HttpServletRequest httpServletRequest;

	public final Supplier<? extends RuntimeException> buildAppException(final String code, final String message,
			Object value, HttpStatus httpStatus, final String... propertiesToBeDotSeperated) throws Exception {
		return () -> {
			// @formatter:off
            ErrorModel errorModel = ErrorModel.builder()
                    .code(code)
                    .message(message)
                    .value(value)
                    .timestamp(Timestamp.valueOf(LocalDateTime.now()).getTime())
                    .status(httpStatus.value())
                    .property(buildDotSepratedString(propertiesToBeDotSeperated))
                    .path(httpServletRequest.getRequestURI())
                    .method(httpServletRequest.getMethod())
                    .build();
            // @formatter:on
			throw AppException.builder().errors(errorModel).build();
		};
	}

	public String buildDotSepratedString(final String... varArgs) {
		String result = Constants.EMPTY_STRING;
		if (ArrayUtils.isNotEmpty(varArgs)) {
			result = String.join(Constants.DOT, varArgs);
		}

		return result;
	}

	public ResponseModel buildResponseModel(Object result, Object count, Object totalCount) {
		count = Optional.ofNullable(count).orElse("-1");
		totalCount = Optional.ofNullable(totalCount).orElse("-1");
		return ResponseModel.builder().result(result).count(Long.valueOf(count.toString()))
				.totalCount(Long.valueOf(totalCount.toString())).build();
	}

	public Long calculateDuration(Date fromDate, Date toDate) {
		LocalDateTime fromLocalDate = fromDate.toInstant().atZone(ZoneId.of(Constants.ASIA_KOLKATA_TIME_ZONE_ID))
				.toLocalDateTime();
		LocalDateTime toLocalDate = toDate.toInstant().atZone(ZoneId.of(Constants.ASIA_KOLKATA_TIME_ZONE_ID))
				.toLocalDateTime();
		Duration duration = Duration.between(fromLocalDate, toLocalDate);
		return duration.toDays();
	}

}