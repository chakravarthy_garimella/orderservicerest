package com.my.cart.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorsEnum {

	// @formatter:off
    BAD_REQUEST_EXCEPTION("MYCART1", "Bad request exception"),
    ERROR_WHILE_PARSING_ERROR_MESSAGE("MYCART2", "Error while parsing error message"),
    ERROR_GENERATING_PRIMARY_KEY_GENERATING_USING_UUID("MYCART3", "Error while generating primary key. Generating random number using UUID"),
    NULL_OBJECT("MYCART4", "Object can not be null"),
    OBJECT_NOT_FOUND("MYCART5", "Object not found"),
    NULL_PROPERTY("MYCART6", "Property can not be null"),
    ;
    // @formatter:on

	private String code;
	private String message;
}
