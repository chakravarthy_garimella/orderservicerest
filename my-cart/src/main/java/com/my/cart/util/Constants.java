package com.my.cart.util;

public final class Constants {

	private Constants() {
	}

	public static final String EMPTY_STRING = "";
	public static final String DOT = ".";
	public static final String DATE_FORMAT_FOR_JSON = "dd-MM-yyyy";
	public static final String DATE_FORMAT_FOR_FILES_API_JSON = "dd-MMM-yyyy";
	public static final String DATE_TIME_FORMAT_FOR_JSON = "dd-MM-yyyy hh:mm a";
	public static final String ASIA_KOLKATA_TIME_ZONE_ID = "Asia/Kolkata";
	public static final String LOCALE_EN = "en";
	public static final String IST_TIME_ZONE = "IST";
	public static final String ID = "id";
	public static final String USERNAME = "username";
	public static final String CORRELATION_ID = "correlation-id";
	public static final String SOURCE = "source";
	public static final String SET_CREATE_DATE_METHOD = "setCreateDate";
	public static final String SET_CREATED_BY_METHOD = "setCreatedBy";
	public static final String SET_UPDATED_DATE_METHOD = "setUpdatedDate";
	public static final String SET_UPDATED_BY_METHOD = "setUpdatedBy";
	public static final String SET_DELETED_METHOD = "setDeleted";
	public static final String POLICE_STATION_MODEL = "policeStationModel";
	public static final String NATURE_MODEL = "natureModel";
	public static final String TYPE_MODEL = "typeModel";
	public static final String FIR_MODEL = "firModel";
	public static final String DISTRICT_MODEL = "districtModel";
	public static final String SECTION_MODEL = "sectionModel";
	public static final String STATUS_MODEL = "statusModel";
	public static final String DISTRICTS_TO_POLICE_STATIONS_BEAN = "districts-to-police-stations-bean";
	public static final String DELETED = "deleted";
	public static final String API = "/api";
	public static final String NO_FILE_EXTENSION = ".noextn";
	public static final String UNKNOWN = "unknown";
	public static final String FIR_ID = "fir-id";
}