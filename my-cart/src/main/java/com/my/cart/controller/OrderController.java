package com.my.cart.controller;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.model.ResponseModel;
import com.my.cart.service.OrderService;
import com.my.cart.service.SearchService;

@RestController
@RequestMapping(value = "/api")
public class OrderController {

	@Autowired
	private OrderService orderService;

	@Autowired
	private SearchService searchService;

	@PostMapping(value = "/v1/persons", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public PersonModel savePersonModel(@RequestBody PersonModel personModel) {
		return orderService.savePersonModel(personModel);
	}

	@PutMapping(value = "/v1/persons", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public PersonModel updatePersonModel(@RequestBody PersonModel personModel) {
		return orderService.updatePersonModel(personModel);
	}

	@DeleteMapping(value = "/v1/persons/{person-id}", produces = APPLICATION_JSON_VALUE)
	public PersonModel deletePersonModel(@PathVariable("person-id") String personId) {
		return orderService.deletePersonModel(personId);
	}

	@GetMapping(value = "/v1/persons", produces = APPLICATION_JSON_VALUE)
	public ResponseModel findPersonModelList() {
		return searchService.findPersonModelList();
	}

	@PostMapping(value = "/v1/orders", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public OrderModel saveOrderModel(@RequestBody OrderModel orderModel) {
		return orderService.saveOrderModel(orderModel);
	}

	@GetMapping(value = "/v1/orders", produces = APPLICATION_JSON_VALUE)
	public ResponseModel findAllOrderModelList() {
		return searchService.findAllOrderModelList();
	}

	@PutMapping(value = "/v1/orders", produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
	public OrderModel updateOrderModel(@RequestBody OrderModel orderModel) {
		return orderService.updateOrderModel(orderModel);
	}

	@GetMapping(value = "/v1/orders/{order-id}", produces = APPLICATION_JSON_VALUE)
	public OrderModel findOrderModelById(@PathVariable("order-id") String orderId) {
		return searchService.findOrderModelById(orderId);
	}

	@DeleteMapping(value = "/v1/orders/{order-id}", produces = APPLICATION_JSON_VALUE)
	public OrderModel deleteOrderModel(@PathVariable("order-id") String orderId) {
		return orderService.deleteOrderModel(orderId);
	}
}