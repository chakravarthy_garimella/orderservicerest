package com.my.cart.service;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.entity.PersonEntity;
import com.my.cart.entity.mongo.ItemMongoEntity;
import com.my.cart.entity.mongo.OrderMongoEntity;
import com.my.cart.entity.mongo.PersonMongoEntity;
import com.my.cart.model.ItemModel;
import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.repository.OrderMongoRepository;
import com.my.cart.repository.PersonMongoRepository;
import com.my.cart.util.Utils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceMongoTest {

	@InjectMocks
	private OrderService orderService;

	@Spy
	private Utils utils;

	@Mock
	private SearchService searchService;

	@Mock
	private PersonMongoRepository personMongoRepository;

	@Mock
	private OrderMongoRepository orderMongoRepository;

	private ItemMongoEntity itemMongoEntity1;
	private ItemMongoEntity itemMongoEntity2;
	private List<ItemMongoEntity> itemMongoEntityList;
	private OrderMongoEntity orderMongoEntity;
	private PersonMongoEntity personMongoEntity;
	private ItemEntity itemEntity1;
	private ItemEntity itemEntity2;
	private List<ItemEntity> itemEntityList;
	private OrderEntity orderEntity;
	private PersonEntity personEntity;
	private PersonModel personModel;
	private OrderModel orderModel;

	@Value("false")
	private Boolean useMongo;

	@Before
	public void setup() {
		ReflectionTestUtils.setField(orderService, "useMongo", true);
		personEntity = PersonEntity.builder().id("1").name("jack").phoneNumber(1234567890L).build();
		personModel = PersonModel.builder().id("1").name("jack").phoneNumber(1234567890L).build();
		itemEntity1 = ItemEntity.builder().id("2").quantity(10L).build();
		itemEntity2 = ItemEntity.builder().id("2").quantity(10L).build();
		itemEntityList = new ArrayList<>();
		itemEntityList.add(itemEntity1);
		itemEntityList.add(itemEntity2);
		orderEntity = OrderEntity.builder().id("2").shippingAddress("address 1").personEntity(personEntity)
				.itemEntityList(itemEntityList).build();

		personMongoEntity = PersonMongoEntity.builder().id("1").name("jack").phoneNumber(1234567890L).build();
		personModel = PersonModel.builder().id("1").name("jack").phoneNumber(1234567890L).build();
		itemMongoEntity1 = ItemMongoEntity.builder().id("2").quantity(10L).build();
		itemMongoEntity2 = ItemMongoEntity.builder().id("2").quantity(10L).build();
		itemMongoEntityList = new ArrayList<>();
		itemMongoEntityList.add(itemMongoEntity1);
		itemMongoEntityList.add(itemMongoEntity2);
		orderMongoEntity = OrderMongoEntity.builder().id("2").shippingAddress("address 1").personEntity(personMongoEntity)
				.itemEntityList(itemMongoEntityList).build();
		ItemModel itemModel1 = ItemModel.builder().id("2").quantity(10L).build();
		ItemModel itemModel2 = ItemModel.builder().id("2").quantity(10L).build();
		List<ItemModel> itemModelList = new ArrayList<>();
		itemModelList.add(itemModel1);
		itemModelList.add(itemModel2);
		orderModel = OrderModel.builder().id("2").shippingAddress("address 1").personModel(personModel)
				.itemModelList(itemModelList).build();
	}

	@After
	public void teardown() {
		itemMongoEntity1 = null;
		itemMongoEntity2 = null;
		itemMongoEntityList = null;
		orderMongoEntity = null;
		personMongoEntity = null;
	}

	@Test
	public void savePersonEntity() {
		Mockito.when(personMongoRepository.save(Mockito.any(PersonMongoEntity.class))).thenReturn(personMongoEntity);
		PersonEntity result = orderService.savePersonEntity(personEntity);
		Assert.assertNotNull(result);
	}

	@Test
	public void savePersonModel() {
		Mockito.when(personMongoRepository.save(Mockito.any(PersonMongoEntity.class))).thenReturn(personMongoEntity);
		PersonModel result = orderService.savePersonModel(personModel);
		Assert.assertNotNull(result);
	}

	@Test
	public void updatePersonModel(){
		Mockito.when(searchService.findPersonEntityById(Mockito.anyString())).thenReturn(personEntity);
		Mockito.when(personMongoRepository.save(Mockito.any(PersonMongoEntity.class))).thenReturn(personMongoEntity);
		PersonModel updatedPersonModel = orderService.updatePersonModel(personModel);
		Assert.assertNotNull(updatedPersonModel);
	}

	@Test
	public void deletePersonModel(){
		String personId = "100";
		Mockito.when(searchService.findPersonEntityById(Mockito.anyString())).thenReturn(personEntity);
		PersonModel deletedPersonModel = orderService.deletePersonModel(personId);
		Assert.assertNotNull(deletedPersonModel);
	}

	@Test
	public void saveOrderEntity(){
		Mockito.when(orderMongoRepository.save(Mockito.any(OrderMongoEntity.class))).thenReturn(orderMongoEntity);
		OrderEntity savedOrderEntity = orderService.saveOrderEntity(orderEntity);
		Assert.assertNotNull(savedOrderEntity);
	}

	@Test
	public void saveOrderModel(){
		Mockito.when(searchService.findPersonEntityById(Mockito.anyString())).thenReturn(personEntity);
		Mockito.when(orderMongoRepository.save(Mockito.any(OrderMongoEntity.class))).thenReturn(orderMongoEntity);
		OrderModel savedOrderModel = orderService.saveOrderModel(orderModel);
		Assert.assertNotNull(savedOrderModel);
	}

	@Test
	public void updateOrderModel(){
		Mockito.when(searchService.findPersonEntityById(Mockito.anyString())).thenReturn(personEntity);
		Mockito.when(orderMongoRepository.save(Mockito.any(OrderMongoEntity.class))).thenReturn(orderMongoEntity);
		OrderModel savedOrderModel = orderService.saveOrderModel(orderModel);
		Assert.assertNotNull(savedOrderModel);
	}

	@Test
	public void deleteOrderModel(){
		Mockito.when(searchService.findOrderEntityById(Mockito.anyString())).thenReturn(orderEntity);
		OrderModel deleteOrderModel = orderService.deleteOrderModel("100");
		Assert.assertNotNull(deleteOrderModel);
	}
}