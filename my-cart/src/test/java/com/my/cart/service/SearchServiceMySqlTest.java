package com.my.cart.service;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.entity.PersonEntity;
import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.model.ResponseModel;
import com.my.cart.repository.*;
import com.my.cart.util.Utils;
import lombok.ToString;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import javax.persistence.ManyToOne;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceMySqlTest {

    @InjectMocks
    private SearchService searchService;

    @Spy
    private Utils utils;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private PersonMongoRepository personMongoRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ItemRepository itemRepository;

    private PersonEntity personEntity;
    private PersonModel personModel;
    private OrderEntity orderEntity;
    private List<OrderEntity> orderEntityList;
    private ItemEntity itemEntity1;
    private ItemEntity itemEntity2;
    private List<ItemEntity> itemEntityList;
    private List<PersonEntity> personEntityList;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(searchService, "useMongo", false);
        personEntity = PersonEntity.builder().id("1").name("jack").phoneNumber(1234567890L).build();
        personModel = PersonModel.builder().id("1").name("jack").phoneNumber(1234567890L).build();
        itemEntity1 = ItemEntity.builder().id("2").quantity(10L).build();
        itemEntity2 = ItemEntity.builder().id("2").quantity(10L).build();
        itemEntityList = new ArrayList<>();
        itemEntityList.add(itemEntity1);
        itemEntityList.add(itemEntity2);
        orderEntity = OrderEntity.builder().id("2").shippingAddress("address 1").personEntity(personEntity)
                .itemEntityList(itemEntityList).build();
        orderEntityList = new ArrayList<>();
        orderEntityList.add(orderEntity);
        personEntityList = new ArrayList<>();
        personEntityList.add(personEntity);
    }

    @After
    public void teardown() {
        personEntity = null;
        personModel = null;
        orderEntity = null;
        orderEntityList = null;
        itemEntity1 = null;
        itemEntity2 = null;
        itemEntityList = null;
        personEntityList = null;
    }

    @Test
    public void findPersonEntityById() {
        Mockito.when(personRepository.findById(Mockito.anyString())).thenReturn(Optional.of(personEntity));
        PersonEntity personEntity = searchService.findPersonEntityById("100");
        Assert.assertNotNull(personEntity);
    }

    @Test
    public void findPersonModelById() {
        Mockito.when(personRepository.findById(Mockito.anyString())).thenReturn(Optional.of(personEntity));
        PersonModel personModel = searchService.findPersonModelById("100");
        Assert.assertNotNull(personModel);
    }

    @Test
    public void findPersonModelList() {
        Mockito.when(personRepository.findAll()).thenReturn(personEntityList);
        ResponseModel responseModel = searchService.findPersonModelList();
        Assert.assertNotNull(responseModel);
    }

    @Test
    public void findAllOrderEntityList(){
        Mockito.when(orderRepository.findAll()).thenReturn(orderEntityList);
        List<OrderEntity> result = searchService.findAllOrderEntityList();
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
    }

    @Test
    public void findAllOrderModelList(){
        Mockito.when(orderRepository.findAll()).thenReturn(orderEntityList);
        ResponseModel responseModel = searchService.findAllOrderModelList();
        Assert.assertNotNull(responseModel);
    }

    @Test
    public void findOrderEntityById(){
        Mockito.when(orderRepository.findById(Mockito.anyString())).thenReturn(Optional.of(orderEntity));
        OrderEntity orderEntity = searchService.findOrderEntityById("100");
        Assert.assertNotNull(orderEntity);
    }

    @Test
    public void findItemEntityById(){
        Mockito.when(itemRepository.findById(Mockito.anyString())).thenReturn(Optional.of(itemEntity1));
        ItemEntity itemEntity = searchService.findItemEntityById("100");
        Assert.assertNotNull(itemEntity);
    }

    @Test
    public void findOrderModelById(){
        Mockito.when(orderRepository.findById(Mockito.anyString())).thenReturn(Optional.of(orderEntity));
        OrderModel orderModel = searchService.findOrderModelById("100");
        Assert.assertNotNull(orderModel);
    }
}