package com.my.cart.service;

import com.my.cart.entity.ItemEntity;
import com.my.cart.entity.OrderEntity;
import com.my.cart.entity.PersonEntity;
import com.my.cart.entity.mongo.ItemMongoEntity;
import com.my.cart.entity.mongo.OrderMongoEntity;
import com.my.cart.entity.mongo.PersonMongoEntity;
import com.my.cart.model.OrderModel;
import com.my.cart.model.PersonModel;
import com.my.cart.model.ResponseModel;
import com.my.cart.repository.*;
import com.my.cart.util.Utils;
import org.apache.commons.collections4.CollectionUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class SearchServiceMongoTest {

    @InjectMocks
    private SearchService searchService;

    @Spy
    private Utils utils;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private PersonMongoRepository personMongoRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private OrderMongoRepository orderMongoRepository;

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private ItemMongoRepository itemMongoRepository;

    private PersonEntity personEntity;
    private PersonModel personModel;
    private OrderEntity orderEntity;
    private List<OrderEntity> orderEntityList;
    private ItemEntity itemEntity1;
    private ItemEntity itemEntity2;
    private List<ItemEntity> itemEntityList;
    private List<PersonEntity> personEntityList;

    private PersonMongoEntity personMongoEntity;
    private OrderMongoEntity orderMongoEntity;
    private List<OrderMongoEntity> orderMongoEntityList;
    private ItemMongoEntity itemMongoEntity1;
    private ItemMongoEntity itemMongoEntity2;
    private List<ItemMongoEntity> itemMongoEntityList;
    private List<PersonMongoEntity> personMongoEntityList;

    @Before
    public void setup() {
        ReflectionTestUtils.setField(searchService, "useMongo", true);
        personEntity = PersonEntity.builder().id("1").name("jack").phoneNumber(1234567890L).build();
        personModel = PersonModel.builder().id("1").name("jack").phoneNumber(1234567890L).build();
        itemEntity1 = ItemEntity.builder().id("2").quantity(10L).build();
        itemEntity2 = ItemEntity.builder().id("2").quantity(10L).build();
        itemEntityList = new ArrayList<>();
        itemEntityList.add(itemEntity1);
        itemEntityList.add(itemEntity2);
        orderEntity = OrderEntity.builder().id("2").shippingAddress("address 1").personEntity(personEntity)
                .itemEntityList(itemEntityList).build();
        orderEntityList = new ArrayList<>();
        orderEntityList.add(orderEntity);
        personEntityList = new ArrayList<>();
        personEntityList.add(personEntity);

        personMongoEntity = PersonMongoEntity.builder().id("1").name("jack").phoneNumber(1234567890L).build();
        itemMongoEntity1 = ItemMongoEntity.builder().id("2").quantity(10L).build();
        itemMongoEntity2 = ItemMongoEntity.builder().id("2").quantity(10L).build();
        itemMongoEntityList = new ArrayList<>();
        itemMongoEntityList.add(itemMongoEntity1);
        itemMongoEntityList.add(itemMongoEntity2);
        orderMongoEntity = OrderMongoEntity.builder().id("2").shippingAddress("address 1").personEntity(personMongoEntity)
                .itemEntityList(itemMongoEntityList).build();
        orderMongoEntityList = new ArrayList<>();
        orderMongoEntityList.add(orderMongoEntity);
        personMongoEntityList = new ArrayList<>();
        personMongoEntityList.add(personMongoEntity);
    }

    @After
    public void teardown() {
        personEntity = null;
        personModel = null;
        orderEntity = null;
        orderEntityList = null;
        itemEntity1 = null;
        itemEntity2 = null;
        itemEntityList = null;
        personEntityList = null;
    }

    @Test
    public void findPersonEntityById() {
        Mockito.when(personMongoRepository.findById(Mockito.anyString())).thenReturn(Optional.of(personMongoEntity));
        PersonEntity personEntity = searchService.findPersonEntityById("100");
        Assert.assertNotNull(personEntity);
    }

    @Test
    public void findPersonModelById() {
        Mockito.when(personMongoRepository.findById(Mockito.anyString())).thenReturn(Optional.of(personMongoEntity));
        PersonModel personModel = searchService.findPersonModelById("100");
        Assert.assertNotNull(personModel);
    }

    @Test
    public void findPersonModelList() {
        Mockito.when(personMongoRepository.findAll()).thenReturn(personMongoEntityList);
        ResponseModel responseModel = searchService.findPersonModelList();
        Assert.assertNotNull(responseModel);
    }

    @Test
    public void findAllOrderEntityList(){
        Mockito.when(orderMongoRepository.findAll()).thenReturn(orderMongoEntityList);
        List<OrderEntity> result = searchService.findAllOrderEntityList();
        Assert.assertTrue(CollectionUtils.isNotEmpty(result));
    }

    @Test
    public void findAllOrderModelList(){
        Mockito.when(orderMongoRepository.findAll()).thenReturn(orderMongoEntityList);
        ResponseModel responseModel = searchService.findAllOrderModelList();
        Assert.assertNotNull(responseModel);
    }

    @Test
    public void findOrderEntityById(){
        Mockito.when(orderMongoRepository.findById(Mockito.anyString())).thenReturn(Optional.of(orderMongoEntity));
        OrderEntity orderEntity = searchService.findOrderEntityById("100");
        Assert.assertNotNull(orderEntity);
    }

    @Test
    public void findItemEntityById(){
        Mockito.when(itemMongoRepository.findById(Mockito.anyString())).thenReturn(Optional.of(itemMongoEntity1));
        ItemEntity itemEntity = searchService.findItemEntityById("100");
        Assert.assertNotNull(itemEntity);
    }

    @Test
    public void findOrderModelById(){
        Mockito.when(orderMongoRepository.findById(Mockito.anyString())).thenReturn(Optional.of(orderMongoEntity));
        OrderModel orderModel = searchService.findOrderModelById("100");
        Assert.assertNotNull(orderModel);
    }
}